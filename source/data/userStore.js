const _ = require('lodash');
const moment = require('moment');
const Sequalize = require('sequelize');
const Op = Sequalize.Op;
const { User, UserQuote, UserAttributes, Rank, Bank } = require('./dbObjects');

const Debug = require('../utilities/debug');
/**
 * 
 * Used to perform operations on user quote related models.
 * 
 * @class UserStore
 */
class UserStore {
    constructor() {
    }
    async createAttributes(username, avatarURL, game, status) {
        return await UserAttributes.create({ currentUserName: username, currentAvatarURL: avatarURL, currentGame: game, currentStatus: status });
    }
    async findUser(userId) {
        const user = await User.findOrCreate({
            where: { userId: userId },
            include: [
                { model: UserAttributes },
                { model: UserQuote, as: 'quotes' },
                { model: Bank, as: 'bank' },
                { model: Rank, as: 'ranks', through: 'userRanks' }
            ]
        });
        return user;
    }
}

module.exports = UserStore;