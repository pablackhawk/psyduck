const _ = require('lodash');
const moment = require('moment');
const Sequalize = require('sequelize');
const Op = Sequalize.Op;
const { User, Bank, BankTransaction, BankItem } = require('./dbObjects');

const Debug = require('../utilities/debug');
/**
 * 
 * 
 * @class BankStore
 */
class BankStore {
    constructor() {
    }
    async addItem(name, description, cost, type, attributes) {
       return await BankItem.create({
           cost: cost,
           name: name,
           description: description,
           type: type,
           dataAttributes: attributes
       });
    }
    async createBank() {
        return await Bank.create({ balance: 0});
    }
    async findBank(userId) {
        const bank = await User.findOrCreate({
            where: { userId: userId },
            include: [{ model: Bank, as: 'bank' }]
        });
        return bank;
    }
}

module.exports = BankStore;