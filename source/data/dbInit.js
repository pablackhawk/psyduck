require('dotenv').config()

const Debug = require('../utilities/debug');
const Sequalize = require('sequelize');

const db = new Sequalize(
    process.env.DB,
    process.env.DB_USER,
    process.env.DB_PASS,
    {
        host: process.env.DB_SERVER,
        port: process.env.DB_PORT,
        dialect: process.env.DB_DIALECT
    });

db.import('models/command');
db.import('models/command_code');
db.import('models/command_response');
db.import('models/user');
db.import('models/user_quote');
db.import('models/bank');
db.import('models/bank_transaction');

Debug.log('Syncing Data Schema');

db.sync().then(result => {
    Debug.log('Schema synced to RDBMS');
}).catch(error => {
    Debug.logError(error);
});;