const Debug = require('../utilities/debug');
const Sequalize = require('sequelize');

const db = new Sequalize(
    process.env.DB,
    process.env.DB_USER,
    process.env.DB_PASS,
    {
        host: process.env.DB_SERVER,
        port: process.env.DB_PORT,
        dialect: process.env.DB_DIALECT,
        logging: false
    });

const Command = db.import('models/command');
const CommandCode = db.import('models/command_code');
const CommandResponse = db.import('models/command_response');
const User = db.import('models/user');
const UserInventory = db.import('models/user_inventory');
const UserQuote = db.import('models/user_quote');
const UserAttributes = db.import('models/user_attributes');
const Bank = db.import('models/bank');
const BankTransaction = db.import('models/bank_transaction');
const BankItem = db.import('models/bank_item.js')
const Rank = db.import('models/rank.js')

Command.hasMany(CommandCode, { as: 'codes' });
Command.hasMany(CommandResponse, { as: 'responses' });

User.hasMany(UserQuote, { as: 'quotes' });
UserQuote.belongsTo(User);

User.hasOne(UserInventory);
User.hasOne(UserAttributes);
User.belongsToMany(Rank, {as: 'ranks', through: 'userRanks'});
Rank.belongsToMany(User, { through: 'userRanks' });

BankItem.belongsToMany(UserInventory, { through: 'userInventoryItems' });

User.hasOne(Bank);
Bank.belongsTo(User);

Bank.hasMany(BankTransaction, { as: 'transactions' });
BankTransaction.belongsTo(Bank);

db.sync().then(result => {
    Debug.log('Schema synced to RDBMS');
}).catch(error => {
    Debug.logError(error);
});;

module.exports = { Command, CommandCode, CommandResponse, User, UserQuote, UserAttributes, Bank, BankTransaction, BankItem, Rank };