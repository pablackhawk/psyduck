const _ = require('lodash');
const moment = require('moment');
const Sequalize = require('sequelize');
const Op = Sequalize.Op;
const { User, UserQuote } = require('./dbObjects');

const Debug = require('../utilities/debug');
/**
 * 
 * Used to perform operations on user quote related models.
 * 
 * @class UserQuoteStore
 */
class UserQuoteStore {
    constructor() {
    }
    /**
     * 
     * 
     * @param {Array} codes - A list of codes for the new command.
     * @param {Array} responses - A list of responses for the new command.
     * @param {string} creator - The originating user who created the command.
     * @memberof UserQuoteStore
     */
    async addQuote(quote, userId, capturerId) {
        try {
            const [existingUser] = await this.findUser(userId);
            const newQuote = await UserQuote.create({ content: quote, capturerId: capturerId });
            await existingUser.addQuote(newQuote);
            return true;
        } catch (error) {
            return false;
        }
    }
    /**
     * 
     * Attempts to find and return a command matching the supplied command codes from the database.
     * 
     * @param {Array} commandCodes - A list of command code strings to match.
     * @returns {Model} - Sequelize Model.
     * @memberof UserCommandStore
     */
    async findUser(userId) {
        const user = await User.findOrCreate({
            where: { userId: userId },
            include: [{ model: UserQuote, as: 'quotes' }]
        });
        return user;
    }
}

module.exports = UserQuoteStore;