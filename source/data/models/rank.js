module.exports = (sequelize, DataTypes) => {
    return sequelize.define('rank', {
        name: DataTypes.STRING,
        description: DataTypes.TEXT,
        requiredExp: DataTypes.INTEGER,
        associatedRole: DataTypes.STRING
    });
}