module.exports = (sequelize, DataTypes) => {
    return sequelize.define('userAttributes', {
        currentUserName: DataTypes.TEXT,
        currentAvatarUrl: DataTypes.TEXT,
        currentGame: DataTypes.TEXT,
        currentStatus: DataTypes.TEXT
    });
}