module.exports = (sequelize, DataTypes) => {
    return sequelize.define('bankTransaction', {
        notes: DataTypes.TEXT,
        type: DataTypes.INTEGER,
        amount: DataTypes.INTEGER
    });
}