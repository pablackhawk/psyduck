module.exports = (sequelize, DataTypes) => {
    return sequelize.define('commandCode', {
        code: DataTypes.STRING
    });
}