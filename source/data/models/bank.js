module.exports = (sequelize, DataTypes) => {
    return sequelize.define('bank', {
        balance: DataTypes.INTEGER
    });
}