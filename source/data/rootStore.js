const UserCommandStore = require('./userCommandStore');
const UserQuoteStore = require('./userQuoteStore');
const UserStore = require('./userStore');
const BankStore = require('./bankStore');
const RankStore = require('./rankStore');
/**
 * 
 * The Root data store to be passed through each middleware. 
 * User destructuring to extract the required stores in each middleware.
 * 
 * @class RootStore
 * @property {UserCommandStore} userCommands - Data store containing dynamic user command operations.
 */
class RootStore {
    constructor() {
        this.userCommands = new UserCommandStore();
        this.userQuotes = new UserQuoteStore();
        this.bankStore = new BankStore();
        this.userStore = new UserStore();
        this.rankStore = new RankStore();
    }

    static store() {
        return new RootStore();
    }
}

module.exports = RootStore;