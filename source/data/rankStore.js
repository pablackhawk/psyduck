const _ = require('lodash');
const moment = require('moment');
const Sequalize = require('sequelize');
const Op = Sequalize.Op;
const { Rank } = require('./dbObjects');

const Debug = require('../utilities/debug');
/**
 * 
 * 
 * @class RankStore
 */
class RankStore {
    constructor() {
    }
    async addRank(name, description, requiredExp, associatedRole) {
       return await Rank.create({
           name: name,
           description: description,
           requiredExp: requiredExp,
           associatedRole: associatedRole
       });
    }
    async findRank(name) {
        const rank = await Rank.findOne({
            where: { name: name }
        });
        return rank;
    }
}

module.exports = RankStore;