require('dotenv').config()
const Debug = require('./utilities/debug');
const Discord = require('discord.js');
// Bot
const Bot = require('./bot');
// Bot Middleware
const loggerMiddleware = require('./middleware/logger');
const eulaMiddleware = require('./middleware/eula');
const commandMiddleware = require('./middleware/commands');
const adminMiddleware = require('./middleware/adminCommands');
const quotesMiddleware = require('./middleware/quotes');
const Jukebox = require('./middleware/jukebox');
const Bank = require('./middleware/bank');

/**
 * 
 * 
 * @class App
 */
class App {
    constructor() {
        Debug.log('Psyduck Boot Initiated');
        Debug.log('Creating Discord.js client instance');
        this.client = new Discord.Client();
        Debug.log('Discord.js client instantiated');
        Debug.log('Creating Bot instance');
        this.bot = new Bot();
        Debug.log('Bot instantiated');


        this.configureBot();
        this.configureClient();
        this.startClient();
    }
    /**
     * 
     * 
     * @memberof App
     */
    configureBot() {
        Debug.log('Configuring bot');
        this.bot.use(loggerMiddleware);
        this.bot.use(eulaMiddleware);
        this.bot.use(adminMiddleware);
        this.bot.use(commandMiddleware);
        this.bot.use(quotesMiddleware);
        Debug.log('Bot configured');
    }
    /**
     * 
     * 
     * @memberof App
     */
    clientReady() {
        Debug.log('Discord.js client ready!');
        this.bot.add('jukebox', new Jukebox(this.client));
        this.bot.add('bank', new Bank(this.client), true);
    }
    /**
     * 
     * 
     * @memberof App
     */
    configureClient() {
        Debug.log('Configuring client');
        this.client.on('ready', () => {
            this.clientReady();
        });
        this.client.on('message', message => this.bot.onMessage(message));
        Debug.log('Client configured');
    }
    /**
     * 
     * 
     * @memberof App
     */
    async startClient() {
        Debug.log('Logging in to server');
        try {
            await this.client.login(process.env.BOT_TOKEN);
            Debug.log('Attached to server');
        } catch (error) {
            Debug.logWarning('Error connecting to server, check configuration');
            Debug.logError(`Error: [${error.message}]`);
        }
    }
}

module.exports = App;