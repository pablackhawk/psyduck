const Debug = require('../../utilities/debug');
const config = require('../../config.json');

const ytdl = require('ytdl-core');
const streamOptions = { seek: 0, volume: 1 };

/**
 * 
 * Class used to add music playing capabilities to the bot.
 * 
 * @class Jukebox
 */
class Jukebox {
    constructor(discordClient) {
        this.client = discordClient;
        this.client.on('message', message => this.handleRequest(message));
        this.joinMusicChannel();

        this.playStack = [];
    }
    /**
     * 
     * @todo Extract this to a global utility.
     * @todo Handle command arguments more elegantly.
     * 
     * @static
     * @param {string} prefix - Command prefix as set in config.js
     * @param {string} messageContent - Raw content from message.
     * @returns {CommandParts} commandParts - An object containing the raw command and a list of arguments passed.
     * 
     * @memberof Jukebox
     */
    commandParts(prefix, messageContent) {
        // First split of raw message. Trim prefix, split on first space to get command code and all arguments.
        let commandArgs = messageContent.slice(prefix.length).split(/\s(.+)/);
        // Extract command code from array.
        const command = commandArgs.shift();
        // Return object for consumption.
        var queueList = [];
        if (commandArgs[0]) {
            queueList = commandArgs[0].split(',');
        }
        return { code: command, args: queueList };
    }
    /**
     * 
     * Method to route a music player command.
     * 
     * @param {object} message - Message from Discord
     * @memberof Jukebox
     */
    handleRequest(message) {
        if (message.author.bot) {
            Debug.logWarning('Message is from bot user, ignoring.');
            return;
        }
        if (message.content.startsWith(config.commandPrefix)) {
            Debug.log(`Message starts with command prefix...`);
            Debug.log(`Processing command...`);
            const rawCommand = this.commandParts(config.commandPrefix, message.content);
            Debug.logInfo(`[Jukebox] Command code: ${rawCommand.code}`);
            if (rawCommand.args.length > 0) { Debug.logInfo(`[Jukebox] Command args: ${rawCommand.args}`); }

            if (rawCommand.code === 'queue') {
                this.queue(rawCommand);
            } else if (rawCommand.code === 'skip') {
                if (this.currentStream) {
                    if (this.currentStream) {
                        this.currentStream.end('skip');
                    }
                }
            } else if (rawCommand.code === 'stop') {
                if (this.currentStream) {
                    this.playStack = [];
                    this.currentStream.end('stop');
                }
            }
        }
    }
    /**
     * 
     * Method to add items to the playStack. If no items exist, start playing.
     * 
     * @param {CommandParts} rawCommand - Parsed command and arguments from the request.
     * @memberof Jukebox
     */
    queue(rawCommand) {
        Debug.log(`Jukebox Queue`);

        this.playStack.push(...rawCommand.args);

        if (!this.currentStream || this.currentStream.destroyed) {
            this.playNext();
        }
    }
    /**
     * 
     * Method to play the next song in the play stack.
     * 
     * @memberof Jukebox
     */
    playNext() {
        if (this.playStack.length > 0) {
            const stream = ytdl(this.playStack.shift());
            this.currentStream = this.musicChannelConnection.playStream(stream, streamOptions);
            this.currentStream.on('end', () => {
                this.playNext();
            });
        }
    }
    /**
     * 
     * Method used to join the configured music channel after the client has connected.
     * 
     * @memberof Jukebox
     */
    async joinMusicChannel() {
        const channel = this.client.channels.get(config.musicChannelId);
        try {
            this.musicChannelConnection = await channel.join();
        } catch (error) {
            Debug.logError(error.message);
        }
    }
}

module.exports = Jukebox;