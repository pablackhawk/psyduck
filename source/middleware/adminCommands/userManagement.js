const Debug = require('../../utilities/debug');

/**
 * 
 * Class used to apply server administration to target users.
 * 
 * @class UserManagement
 */
class UserManagement {
    /**
     * 
     * Ban user from the server.
     * 
     * @static
     * @param {object} discordMessage - Message received from Discord.
     * @param {RootStore} rootStore - Root data store containing members of all substores.
     * @param {Array} options - Collection of options used to add a command. Index 0 = userId, index 1 = reason for ban.
     * @memberof UserManagement
     */
    static async ban(discordMessage, rootStore, options) {
        let [
            userId,
            reason
         ] = options;
        const guild = discordMessage.member.guild;
        if (guild) {
            try {
                if(userId.startsWith('<@')){
                    userId = userId.match(/\<@(.*?)\>/)[1];
                    userId = userId.replace('!','');
                }
                const user = await guild.ban(userId);
                discordMessage.channel.send(`Banned ${user.username || user.id || user} from ${guild} ${!reason ? '' : `for ${reason}`}`);
            } catch (error) {
                Debug.logError(error);
            }
        }
    }
    /**
     * 
     * Remove a ban from a user for the server.
     * 
     * @static
     * @param {object} discordMessage - Message received from Discord.
     * @param {RootStore} rootStore - Root data store containing members of all substores.
     * @param {Array} options - Collection of options used to add a command. Index 0 = userId, index 1 = reason for ban removal.
     * @memberof UserManagement
     */
    static async removeBan(discordMessage, rootStore, options) {
        let [
            userId,
            reason
         ] = options;
        const guild = discordMessage.member.guild;
        if (guild) {
            try {
                if(userId.startsWith('<@')){
                    userId = userId.match(/\<@(.*?)\>/)[1];
                    userId = userId.replace('!','');
                }
                const user = await guild.unban(userId);
                discordMessage.channel.send(`Removed ban for user ${user.username || user.id || user} from ${guild} ${!reason ? '' : `for ${reason}`}`);
            } catch (error) {
                Debug.logError(error);
            }
        }
    }
}

module.exports = UserManagement;