const moment = require('moment');
const Debug = require('../../utilities/debug');

/**
 * 
 * Class used to route CRUD operations to the UserCommandStore.
 * 
 * @class BankManagement
 */
class BankManagement {

    static async addBalance(discordMessage, rootStore, options) {
        const [amount] = options;
        let [userId] = options[1];
        if (userId.startsWith('<@')) {
            userId = userId.match(/\<@(.*?)\>/)[1];
            userId = userId.replace('!', '');
        }

        const { bankStore } = rootStore;
        const [ user ] = await bankStore.findBank(userId);
        let userBank = user.bank;
        if(!userBank) {
            const newBank = await bankStore.createBank();
            userBank = await user.setBank(newBank);
        }

        userBank.balance += parseInt(amount);
        await userBank.save();

        discordMessage.channel.send(`Added ${amount} DuckyCoins to <@${userId}>'s bank.`);
    }
    static async addItem(discordMessage, rootStore, options) {
        const {
            bankStore
        } = rootStore;

        const [
            itemName,
            itemData
        ] = options;

        const [description, cost, type, attributes] = itemData;

        try {
            const item = await bankStore.addItem(itemName, description, cost, type, attributes);

            let attributeString = '';
            const att = JSON.parse(attributes);
            for(const prop in att) {
                attributeString += `${prop} - ${att[prop]}\n`;
            }

            const embed = {
                'description': description,
                'color': 6991252,
                'timestamp': moment(),
                'author': {
                    'name': itemName,
                    'icon_url': 'https://cdn.discordapp.com/embed/avatars/0.png'
                },
                'fields': [
                    {
                        'name': 'Cost',
                        'value': cost
                    },
                    {
                        'name': 'Type',
                        'value': type
                    },
                    {
                        'name': 'Attributes',
                        'value': `\`\`\`${attributeString}\`\`\``
                    }
                ]
            };

            discordMessage.channel.send('Added Item to Bank', { embed });
        } catch (error) {
            Debug.logError(error);
        }
    }
}

module.exports = BankManagement;