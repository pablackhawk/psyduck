const _ = require('lodash');
const config = require('../../config.json');
const Debug = require('../../utilities/debug');

const UserManagement = require('./userManagement');
const CommandManagement = require('./commandManagement');
const BankManagement = require('./bankManagement');
const RankManagement = require('./rankManagement');

const commands = {
    ban: UserManagement.ban,
    unban: UserManagement.removeBan,
    addcommand: CommandManagement.addCommand,
    addcodes: CommandManagement.addCodes,
    addresponses: CommandManagement.addResponses,
    additem: BankManagement.addItem,
    addbalance: BankManagement.addBalance,
    addrank: RankManagement.addRank,
    assignrank: RankManagement.assignRank,
    assignall: RankManagement.assignRankAll
};

const ignorableCommands = ["queue", "stop", "skip", "quote", "random", "balance", "join"];

/**
 * 
 * Middleware responsible for handling all server administration commands.
 * 
 * @class AdminCommands
 */
class AdminCommands {

    /**
     * @typedef {object} CommandParts
     * @property {string} code - The raw command code stripped of any prefixes.
     * @property {Array} args - A list of command args. Destructuring up to consuming function. 
     */

    /**
     * 
     * @todo Extract this to a global utility.
     * @todo Handle command arguments more elegantly.
     * 
     * @static
     * @param {string} prefix - Command prefix as set in config.js
     * @param {string} messageContent - Raw content from message.
     * @returns {CommandParts} commandParts - An object containing the raw command and a list of arguments passed.
     * @memberof AdminCommands
     */
    static commandParts(prefix, messageContent) {
        // First split of raw message. Trim prefix, split on first space to get command code and all arguments.
        let commandArgs = messageContent.slice(prefix.length).split(/\s(.+)/);
        // Extract command code from array.
        const command = commandArgs.shift();
        // Second split will create an arrau containing first and second arguments. e.g. a command code and a list of responses.
        commandArgs = commandArgs[0].split(/\s(.+)/);
        // Create parts array with first argument and second arguments split by comma. Used for destructuring later in lifecycle.
        const parts = [commandArgs[0]];
        if (commandArgs[1]) {
            parts.push(commandArgs[1].split('|'));
        }
        // Return object for consumption.
        return { code: command, args: parts };
    }
    /**
     * 
     * 
     * @static
     * @param {object} message - Message received from Discord.
     * @param {RootStore} rootStore - Root data store containing members of all substores.
     * @returns {function} - Function returned to pass in next function from the middleware stack.
     * @memberof AdminCommands
     */
    static adminCommand(message, rootStore) {
        return function (next) {
            Debug.log(`Begin handle admin command`);

            if (message.author.bot) {
                Debug.logWarning('Message is from bot user, ignoring.');
                next(message, rootStore);
                return;
            }

            const isBotAdmin = !!message.member.roles.get(config.botAdminRoleId);
            const adminPrefixPresent = message.content.startsWith(config.adminCommandPrefix);

            if (!adminPrefixPresent) {
                Debug.logError(`Message does not start with command prefix ${config.adminCommandPrefix}`);
                Debug.log(`End handle admin command`);
                next(message, rootStore);
                return;
            }

            if (isBotAdmin) {
                Debug.log(`Processing command...`);
                const rawCommand = AdminCommands.commandParts(config.adminCommandPrefix, message.content);
                Debug.logInfo(`[Admin] Command code: ${rawCommand.code}`);
                if (rawCommand.args.length > 0) { Debug.logInfo(`[Admin] Command args: ${rawCommand.args}`); }

                if (_.indexOf(ignorableCommands, rawCommand.code) > -1) {
                    Debug.logWarning(`Command with code [${rawCommand.code}] is ignorable; skipping.`);
                    next(message, rootStore);
                    return;
                }

                const command = commands[rawCommand.code];
                if (command) {
                    command(message, rootStore, [...rawCommand.args])
                }

            } else {
                Debug.logError(`User is not a bot admin ${message.author.username}`);
                message.channel.send(`<@${message.author.id}>, buddy. Do you even admin?`);
                message.channel.send(`**NOPE**`);
                Debug.log(`End handle admin command`);
            }
            next(message, rootStore);
        }
    }
}

module.exports = AdminCommands.adminCommand;

