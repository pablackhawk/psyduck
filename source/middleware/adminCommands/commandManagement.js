const Debug = require('../../utilities/debug');
/**
 * 
 * Class used to route CRUD operations to the UserCommandStore.
 * 
 * @class CommandManagement
 */
class CommandManagement {
    /**
     * 
     * Add a new command to the database for use by server users.
     * 
     * @static
     * @param {object} discordMessage - Message received from Discord.
     * @param {RootStore} rootStore - Root data store containing members of all substores.
     * @param {Array} options - Collection of options used to add a command. Index 0 = code, index 1 = list of command response strings.
     * @memberof CommandManagement
     */
    static async addCommand(discordMessage, rootStore, options) {
        const {
            userCommands
        } = rootStore;

        const [
            code,
            responses
        ] = options;

        try {
            await userCommands.addCommand([code], [...responses], discordMessage.member.id);
        } catch (error) {
            Debug.logError(error);
        }
    }
    /**
     * 
     * Add alias codes to an existing command.
     * 
     * @static
     * @param {object} discordMessage - Message received from Discord.
     * @param {RootStore} rootStore - Root data store containing members of all substores.
     * @param {Array} options - Collection of options used to add codes to a command. Index 0 = existing code, index 1 = list of command code strings. 
     * @memberof CommandManagement
     */
    static async addCodes(discordMessage, rootStore, options) {
        const {
            userCommands
        } = rootStore;

        let [
            code,
            newCodes
        ] = options;

        try {
            await userCommands.updateCommand(code, { codes: [...newCodes], responses: [] });
        } catch (error) {
            Debug.logError(error);
        }
    }
    /**
     * 
     * Add responses to an existing command.
     * 
     * @static
     * @param {object} discordMessage - Message received from Discord.
     * @param {RootStore} rootStore - Root data store containing members of all substores.
     * @param {Array} options - Collection of options used to add codes to a command. Index 0 = existing code, index 1 = list of command response strings. 
     * @memberof CommandManagement
     */
    static async addResponses(discordMessage, rootStore, options) {
        const {
            userCommands
        } = rootStore;

        let [
            code,
            newResponses
        ] = options;

        try {
            await userCommands.updateCommand(code, { codes: [], responses: [...newResponses] });
        } catch (error) {
            Debug.logError(error);
        }
    }
}

module.exports = CommandManagement;