const Debug = require('../../utilities/debug');

class Eula {
    static middleware(message, rootStore) {
        return function (next) {
            Eula.updateUser(message, rootStore);
            next(message, rootStore);
        }
    }
    static async updateUser(message, rootStore) {
        try {
            if (message.content === '!join') {
                const serverRole = message.guild.roles.find('name', 'Authenticated User');
                if (serverRole) {
                    const guildUser = await message.guild.fetchMember(message.author.id);
                    if (guildUser && !guildUser.roles.get(serverRole.id)) {
                        guildUser.addRole(serverRole);
                        //message.channel.send(`Added rank ${rank} to user <@${userId}>. Thanks!`);
                    }
                }
            }
        } catch (error) {
            Debug.logError(error);
        }
    }
}

module.exports = Eula.middleware;