const Debug = require('../../utilities/debug');

class Logger {
    static middleware(message, rootStore) {
        return function (next) {
            Logger.updateUser(message, rootStore);
            next(message, rootStore);
        }
    }
    static async updateUser(message, rootStore) {
        try {
            const { userStore } = rootStore;
            const [ user, created ] = await userStore.findUser(message.author.id);
            let userAtt = user.userAttribute;
            const serverUser = message.member.user;
            if (!userAtt) {
                const newAttributes = await userStore.createAttributes(serverUser.username, serverUser.avatarURL, serverUser.presence.game, serverUser.presence.status);
                userAtt = await user.setUserAttribute(newAttributes);
            } else {
                userAtt.currentUserName = serverUser.username;
                userAtt.currentAvatarUrl = serverUser.avatarURL;
                userAtt.currentGame = serverUser.presence.game;
                userAtt.currentStatus = serverUser.presence.status;

                await userAtt.save();
            }
        } catch (error) {
            Debug.logError(error);
        }
    }
}

module.exports = Logger.middleware;