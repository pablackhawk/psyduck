const rootStore = require('../data/rootStore').store();
/**
 * 
 * The Bot class is used to pipe all incoming Messages through configured middleware.
 *
 * @class Bot
 * @property {Array} fns - Collection of functions making up the middleware stack.
 */
class Bot {
    constructor() {
        this.fns = [];
    }
    /**
     * 
     * When a message is recieved it will be passed through all middleware in the stack.
     * 
     * @param {Message} message - The Discord Message emitted from the 'message' event of the Discord.js Client.
     * @memberof Bot
     */
    onMessage(message) {
        const next = () => {};
        for(let i = 0; i < this.fns.length; i++) {
            const fn = this.fns[i];
            fn(message, rootStore)(this.fns[i + 1] || next);
        }
    }
    /**
     * 
     * Function called when adding middleware to the stack.
     * 
     * @param {function} fn - The function to be added to the middleware stack.
     * @returns {Bot} - Returns Bot instance for fluent api.
     * @memberof Bot
     */
    use(fn) {
        this.fns.push(fn);
        return this;
    }
    /**
     * 
     * Function called when attaching members to an instance of Bot dynamically.
     * This may be used when more complex workflows than stateless functions are required.
     * 
     * @param {string} key - Name or key of the object to be attached.
     * @param {object} object - Object to be attached.
     * @memberof Bot
     */
    add(key, object, injectStore = false) {
        if(injectStore) {
            object.rootStore = rootStore;
        }
        this[key] = object;
    }
}

module.exports = Bot;