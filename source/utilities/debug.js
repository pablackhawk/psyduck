const colors = require('colors');
const moment = require('moment');

/**
 * 
 * 
 * @class Debug
 */
class Debug {
    /**
     * 
     * 
     * @static
     * @param {any} message 
     * @param {string} [padding='----'] 
     * @param {boolean} [timestamp=true] 
     * @returns {string} Formatted content string.
     * @memberof Debug
     */
    static buildContent(message, padding = '----', timestamp = true) {
        return `${timestamp ? `[${moment()}]` : ''} ${padding} ${message} ${padding}`;
    }
    /**
     * 
     * 
     * @static
     * @param {any} message 
     * @param {string} [padding='----'] 
     * @param {boolean} [timestamp=true] 
     * @memberof Debug
     */
    static log(message, padding = '----', timestamp = true) {
        console.log(this.buildContent(message, padding, timestamp).green);
    }
    /**
     * 
     * 
     * @static
     * @param {any} message 
     * @param {string} [padding='----'] 
     * @param {boolean} [timestamp=true] 
     * @memberof Debug
     */
    static logWarning(message, padding = '----', timestamp = true) {
        console.log(this.buildContent(message, padding, timestamp).yellow);
    }
    /**
     * 
     * 
     * @static
     * @param {any} message 
     * @param {string} [padding='----'] 
     * @param {boolean} [timestamp=true] 
     * @memberof Debug
     */
    static logError(message, padding = '----', timestamp = true) {
        console.log(this.buildContent(message, padding, timestamp).red);
    }
    /**
     * 
     * 
     * @static
     * @param {any} message 
     * @param {string} [padding='----'] 
     * @param {boolean} [timestamp=true] 
     * @memberof Debug
     */
    static logInfo(message, padding = '----', timestamp = true) {
        console.log(this.buildContent(message, padding, timestamp).blue);
    }

}

module.exports = Debug;