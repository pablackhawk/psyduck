const expect = require('chai').expect;
const Bot = require('../source/bot');

describe('Bot()', () => {
    describe('use(fn)', () => {
        it('Should add functions to the stack', () => {
            // Arrange
            const bot = new Bot();
            const fn1 = () => {};
            const fn2 = () => {};
            const fn3 = () => {};
            // Act
            bot.use(fn1).use(fn2).use(fn3);
            // Assert
            expect(bot.fns.length).to.equal(3);
        });
    });
    describe('add(key, object)', () => {
        it('Should add member to Bot instance', () => {
            // Arrange
            const bot = new Bot();
            const fn1 = () => {};
            const key = 'newProp';
            // Act
            bot.add(key, fn1);
            // Assert
            expect(bot.newProp).to.not.be.undefined;
        });
    });
});